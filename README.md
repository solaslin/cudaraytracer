![ALT](/media/images/raytrace_v1.png "CUDA RayTracing")

# CUDA Ray Tracer
_RookiesEngine and CUDA RayTracer v1_

My CUDA ray-tracer practice:
- Environment: VS2015 and CUDA 10.2
- Click the CUDARayTracer.exe under bin directory to run
- Press F1, F2 to rotate camera
- Press F3, F4 to rotate light
- Press F5, F6 to move sphere