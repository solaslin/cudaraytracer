/**
*
* @file     EnumDef.h
* @author   Ethan Lin
* @version  1.0
*
* @section  LICENSE
*/

#ifndef __ENUM_DEF__
#define __ENUM_DEF__


#if defined __EXPORT_RKSENGINE_DLL__
#define ROOKIES_ENGINE_API __declspec(dllexport)
#elif defined __USE_RKSENGINE_DLL__
#define ROOKIES_ENGINE_API __declspec(dllimport)
#else
#define ROOKIES_ENGINE_API
#endif

#ifndef __SET_DEBUG__
#define CHECK_GL_ERROR()
#else
#define CHECK_GL_ERROR() \
{ \
GLenum err = glGetError(); \
if (err != GL_NO_ERROR) \
{	\
printf("GLError: %x, at %s, line %d\n",err,__FILE__, __LINE__-1);   \
abort(); \
} \
}
#endif //__SET_DEBUG__

#define VertAttrType int
#define VertLayoutType int

namespace ROOKIES3D
{
	//typedef enum
	//{
	//	GA_OPENGL = 0,
	//	GA_D3D12 = 1,
	//} EGraphicAPI;

	typedef enum
	{
		CT_RGB = 0,
		CT_BGR = 1,
		CT_RGBA = 2,
		CT_BGRA = 3,
		CT_ARGB = 4
	} EColorType;

	typedef enum
	{
		MESH_UNKNOWN = 0,
		MESH_RAWDATA = 1,
		MESH_OBJ = 2,
		MESH_FBX = 3
	} EMeshType;

	typedef enum
	{
		AT_DEGREE = 0,
		AT_RADIAN = 1
	} EAngleType;

	typedef enum
	{
		PT_POINTS = 0,
		PT_TRIANGLES = 1
	} EPrimitiveType;

	namespace BuiltInVertAttr
	{
		const VertAttrType POS = 0;
		const VertAttrType UV = 1;
		const VertAttrType NORMAL = 2;
		const VertAttrType TANGENT = 3;
		const VertAttrType TOTAL_BUILT_IN_TYPE = 4;
	}

	namespace BuiltInVertLayout
	{
		const VertLayoutType POS = 0;
		const VertLayoutType POS_UV = 1;
		const VertLayoutType POS_UV_NORMAL = 2;
		const VertLayoutType POS_UV_NORMAL_TANGENT = 3;
		const VertLayoutType CUSTOM_TYPE_START = 32;
	}
}

// Build in Binding
//static VertAttrBinding PosRegBinding("inPos", 0);
//static VertAttrBinding UVRegBinding("inUV", 1);
//static VertAttrBinding NormalRegBinding("inNormal", 2);
//static VertAttrBinding TangentRegBinding("inTangent", 3);

#endif	// endif __ENUM_DEF__
