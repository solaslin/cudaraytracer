/**
*
* @file     IRenderEngine.h
* @author   Ethan Lin
* @version  1.0
*
* @section  LICENSE
*/


#ifndef __I_RENDER_ENGINE_H__
#define __I_RENDER_ENGINE_H__

namespace ROOKIES3D
{

class ROOKIES_ENGINE_API IRenderEngine
{
public:
	virtual ~IRenderEngine() {};
	virtual GLFWwindow* CreateContext(int width, int height, int antiAliasing = 1) = 0;	
	virtual IGraphics* GetGraphicsHandler() = 0;
	virtual void SetResourceRootPath(const char* rootPath) = 0;

	// vertex declaration
	virtual VertexAttribute* CreateAttributeBinding(const char* name, int loc, int numComp, int compType, bool normalized, int totalSize) = 0;
	virtual VertexLayout* CreateNewVertexLayout(int numAttr, ... ) = 0;

	//
	// engine class:
	//
	// material
	virtual IMaterial* CreateMaterial() = 0;
	virtual IMaterial* CreateMaterialFromFile(const char* matFileName) = 0;

	// geometry
	virtual IGeometry* CreateGeomtry(VertLayoutType type, bool dynamicVert, bool dynamicIdx) = 0;
	virtual IGeometry* GetScreenQuad(bool flipY) = 0;
	virtual IGeometry* GetMirrorQuad() = 0;
	virtual IModel* CreateEmptyModel() = 0;	
	virtual IModel* CreateModelFromObjFile(const char* objFile, VertLayoutType type, bool fromRHCoord = true) = 0;

	// render target
	virtual IRenderTarget* CreateRenderTarget(int width, int height) = 0;
	virtual void SetRenderToTarget(IRenderTarget* rt) = 0;
	virtual void ForceSwitchRenderTarget(IRenderTarget* rt) = 0;

	// camera
	virtual IViewCamera* CreateCamera() = 0;
	virtual IViewCamera* GetMainCamera() = 0;
	virtual void SetMainCamera(IViewCamera* cam) = 0;

	// post effect
	virtual IPostEffectManager* CreatePostEffectManager() = 0;

	// light
	virtual ILightManager* GetLightManager() = 0;

	// render flow
	//virtual void SetToFixedFunction() = 0;		// not supported in OGLES
    virtual void BeginRender() = 0;
    virtual void EndRender() = 0;
};

}

#endif
