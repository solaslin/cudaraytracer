/**
*
* @file     Geometry.h
* @author   Ethan Lin
* @version  1.0
*
* @section  LICENSE
*/
#ifndef __GEOMETRY_H__
#define __GEOMETRY_H__

namespace ROOKIES3D
{

class ROOKIES_ENGINE_API IGeometry
{
public:
    virtual ~IGeometry() {};
	virtual VertLayoutType GetVertType() = 0;
	virtual const char* GetName() = 0;
	//virtual bool IsVisible() = 0;
	//virtual void SetVisible(bool flag) = 0;
	
	virtual void Draw(EPrimitiveType mode = PT_TRIANGLES) = 0;
	virtual void RefreshGeometryData() = 0;
	virtual void UploadVertexBuffer(const float *data, int vertCount, int perVertSize) = 0;
	virtual void UploadIndexBuffer(const unsigned short *data, int idxCount, int perIdxSize) = 0;
	virtual const AABB& GetLocalAABB() = 0;
};

}

#endif
