/**
*
* @file     Graphics.h
* @author   Ethan Lin
* @version  1.0
*
* @section  LICENSE
*/


#ifndef __GRAPHICS_H__
#define __GRAPHICS_H__

namespace ROOKIES3D
{

class IModel;

class ROOKIES_ENGINE_API IGraphics
{
public:
    virtual ~IGraphics() {};
	virtual GLFWwindow* CreateContext(int width, int height, int antiAliasing = 1) = 0;		// to-do: hide

	// some kind of render state....
	virtual void SetClearColor(float r, float g, float b, float a) = 0;
    virtual void SetClearColor(const Vector4& color) = 0;
	virtual void SetRenderState(int stateBits) = 0;
	virtual void SetBlendFactors(int sourceFactor, int destinationFactor) = 0;
	virtual void SetDepthTest(bool testEnable, bool writeEnable, int compareMode) = 0;
	virtual void SetCulling(int faceToCull) = 0;
	virtual void SetColorWrite(bool colorWrite) = 0;
	virtual void SetDepthBias(bool bEnable, float factor, float unit) = 0;

	// frame-buffer
	virtual const unsigned char* GetFrameBufferToByte( IRenderTarget* rt, int* bufferW, int* bufferH, EColorType type = CT_BGRA) = 0;

	// gpu primitive resource
	virtual IProgram* CreateShaderProgramFromFile(const char* vsFileName, const char* fsFileName) = 0;
	virtual IProgram* CreateShaderProgramFromString(const char* vShader, const char* fShader, const char* name) = 0;
	virtual ITexture* CreateTexture(const char* fileName) = 0;

	//
	// engine class:
	//
	// render target
	virtual IRenderTarget* CreateRenderTexture(int width, int height) = 0;	// to-do: hide
	virtual void SetCurrentRT(IRenderTarget* rt) = 0;						// to-do: hide
	virtual void ForceSwitchRenderTarget(IRenderTarget* rt) = 0;			// to-do: hide

	// render flow
	virtual void SetToFixedFunction() = 0;		// not supported in OGLES	// to-do: hide
    virtual void BeginFrame() = 0;											// to-do: hide
    virtual void EndFrame() = 0;											// to-do: hide
	virtual void Draw(EPrimitiveType mode, int numIdx) = 0;					// to-do: hide

	// manually call
	// to-do: get rid of GL type
	virtual void SetProgram(IProgram* program) = 0;
	virtual void SetTexture(int regLoc, int texUnit, ITexture* tex) = 0;
	virtual void SetGeometry(GLuint vBuffer, GLuint iBuffer, const VertexLayout* vertLayout) = 0;

	virtual void SetShaderMatrix(int regLoc, const float* data) = 0;
	virtual void SetShaderDataPointer(int regLoc, const float* data, int count) = 0;
	virtual void UploadVertexBuffer(GLuint vBuffer, const void *data, int count, int perVerSize, bool dynamic) = 0;
	virtual void UploadIndexBuffer(GLuint iBuffer, const void *data, int count, int perIdxSize, bool dynamic) = 0;

	virtual GLuint GetDefaultFrameBuffer() = 0;
	virtual GLuint GetDefaultRenderBuffer() = 0;		
};

}

#endif
