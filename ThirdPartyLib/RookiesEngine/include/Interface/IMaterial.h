/***********************************************
*
* @file     Material.h
* @version  0.1
*
* @section  LICENSE
***********************************************/


#ifndef __MATERIAL_H__
#define __MATERIAL_H__

namespace ROOKIES3D
{

class ROOKIES_ENGINE_API IMaterial
{
public:
	virtual ~IMaterial(){};

	virtual const char* GetName() = 0;
	virtual int GetRenderState() = 0;
	virtual void SetRenderState(int state) = 0;

	virtual void BindProgram(IProgram* prog) = 0;
	virtual void BindTexture(const char* samplerName, ITexture* tex) = 0;

	virtual void BindShaderMatrix(const char* name, Matrix44 &mat) = 0;
	virtual void BindShaderVector(const char* name, Vector2 &vec) = 0;
	virtual void BindShaderVector(const char* name, Vector3 &vec) = 0;
	virtual void BindShaderVector(const char* name, Vector4 &vec) = 0;
	virtual void BindShaderFloat(const char* name, float& value) = 0;

	virtual void BindShaderMatrixValue(const char* name, Matrix44 mat) = 0;
	virtual void BindShaderVectorValue(const char* name, Vector2 vec) = 0;
	virtual void BindShaderVectorValue(const char* name, Vector3 vec) = 0;
	virtual void BindShaderVectorValue(const char* name, Vector4 vec) = 0;
	virtual void BindShaderFloatValue(const char* name, float value) = 0;

	virtual void SetShaderUniforms() = 0;
};

}


#endif