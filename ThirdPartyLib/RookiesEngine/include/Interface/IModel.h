/**
*
* @file     Model.h
* @author   Ethan Lin
* @version  1.0
*
* @section  LICENSE
*/


#ifndef __MODEL__
#define __MODEL__

namespace ROOKIES3D
{

class ROOKIES_ENGINE_API IModel
{

public:
	virtual ~IModel() {};

	// FIXME: need to set a material vector....
	virtual void SetGeometry(IGeometry* geo) = 0;
	virtual void SetMaterial(IMaterial* mat) = 0;	
	virtual bool IsVisible() = 0;
	virtual void SetVisible(bool flag) = 0;
	virtual void Draw(EPrimitiveType mode = PT_TRIANGLES) = 0;

    virtual IGeometry* GetGeometry() = 0;
	virtual IMaterial* GetMaterial() = 0;
	virtual Transform* GetTransform() = 0;
	virtual const AABB& GetLocalAABB() = 0;
};

}

#endif
