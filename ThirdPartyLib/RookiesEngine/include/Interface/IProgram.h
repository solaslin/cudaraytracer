/**
*
* @file     OGLProgram.h
* @author   Ethan Lin
* @version  1.0
*
* @section  LICENSE
*/
#ifndef __OGL_PROGRAM__
#define __OGL_PROGRAM__

namespace ROOKIES3D
{

class ROOKIES_ENGINE_API IProgram
{
public:
	virtual ~IProgram() {};
	virtual const char* GetName() { return _name; };
	virtual int GetRegisterLocation(const char* name) = 0;

protected:
	const char*		_name;
};

}

#endif
