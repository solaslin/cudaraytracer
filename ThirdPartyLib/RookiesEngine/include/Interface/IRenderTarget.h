/**
*
* @file     RenderTarget.h
* @author   Ethan Lin
* @version  1.0
*
* @section  LICENSE
*/
#ifndef __RENDERTARGET_H__
#define __RENDERTARGET_H__

namespace ROOKIES3D
{

class ROOKIES_ENGINE_API IRenderTarget
{
public:
    virtual ~IRenderTarget() {};
    virtual void SetClearColor(float r, float g, float b, float a) = 0;
	virtual void SetClearColor(const Vector4& color) = 0;
	virtual void SetCamera(IViewCamera* cam) = 0;
	
	virtual ITexture* GetColorTexture() = 0;	
	virtual IViewCamera* GetCamera() = 0;	
	virtual int GetWidth() = 0;
	virtual int GetHeight() = 0;
};

}

#endif
