/**
*
* @file     OGLTexture.h
* @author   Ethan Lin
* @version  1.0
*
* @section  LICENSE
*/
#ifndef __OGL_TEXTURE__
#define __OGL_TEXTURE__

namespace ROOKIES3D
{

class ROOKIES_ENGINE_API ITexture
{
public:
	virtual ~ITexture() {};
    
	virtual void UploadTexture(int width, int height, int channels, GLubyte* data, bool isCVMat = false) = 0;
	virtual int GetWidth() { return _width; };
	virtual int GetHeight() { return _height; };
	virtual int GetMemSize() { return _memSize; };
	virtual const char* GetName() { return _name; };

protected:
	const char*	_name;
	int			_width;
	int			_height;
	int			_memSize;
};

}

#endif
