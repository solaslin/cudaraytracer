/**
*
* @file     VertexLayout.h
* @author   Ethan Lin
* @version  1.0
*
* @section  LICENSE
*/
#ifndef __VERTEX_LAYOUT__
#define __VERTEX_LAYOUT__

namespace ROOKIES3D
{

class ROOKIES_ENGINE_API VertexAttribute
{
public:
	virtual ~VertexAttribute() {};
	virtual VertAttrType GetType() const = 0;
	virtual const char* GetName() const = 0;
	virtual int GetRegLoc() const = 0;
	virtual int GetComponentType() const = 0;
	virtual int GetNumComponent() const = 0;
	virtual bool GetIsNormalized() const = 0;
	virtual int GetComponentByteSize() const = 0;
};

class ROOKIES_ENGINE_API VertexLayout
{
public:
	virtual ~VertexLayout() {};
	virtual VertLayoutType GetType() const = 0;
	virtual int GetVertexSizeByte() const = 0;
	virtual const VertexAttribute* GetAttribute(int elemID) const = 0;
	virtual int GetNumAttributes() const = 0;
	virtual const char* GetAttributeName(int elemID) const = 0;
	virtual int GetAttributeRegLoc(int elemID) const = 0;
};

}

#endif
