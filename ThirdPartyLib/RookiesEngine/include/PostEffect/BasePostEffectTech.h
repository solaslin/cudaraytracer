#ifndef __BASE_POSTEFFECTTECH__
#define __BASE_POSTEFFECTTECH__

namespace ROOKIES3D
{

class ROOKIES_ENGINE_API BasePostEffect
{
public:	
	virtual ~BasePostEffect();
	virtual void Init() = 0;
	virtual void Render(IRenderTarget* inputRtt, IRenderTarget* outputRtt) = 0;
	virtual void CheckInit() final;	
	virtual bool IsEnable() final;
	virtual void SetEnable(bool enable) final;
	virtual void SetGraphics(IGraphics* g) final;
	virtual PostEffectType GetPostEffctType() final;

protected:
	PostEffectType			_type;
	bool					_enable = false;
	bool					_inited = false;
	IRenderEngine*			_engine = nullptr;
	IGraphics*				_graphic = nullptr;
	IMaterial*				_material = nullptr;
	IProgram*				_effectShader = nullptr;
	IGeometry*				_renderQuad = nullptr;

	const char* _vsCode;
	const char* _fsCode;
};

}

#endif
