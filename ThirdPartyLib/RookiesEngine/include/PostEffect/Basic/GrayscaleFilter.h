#ifndef __GRAYSCALE_FILTER__
#define __GRAYSCALE_FILTER__

namespace ROOKIES3D
{

class ROOKIES_ENGINE_API GrayscaleFilter : public BasePostEffect
{
public:
	GrayscaleFilter();
	virtual ~GrayscaleFilter() override;

	virtual void Init() override;
	virtual void Render(IRenderTarget* inputRtt, IRenderTarget* outputRtt) override;
};

}
#endif