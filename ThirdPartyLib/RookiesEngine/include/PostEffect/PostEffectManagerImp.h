#ifndef __POSTEFFECTMANAGER_IMP__
#define __POSTEFFECTMANAGER_IMP__

namespace ROOKIES3D
{

class RenderEngineImp;
//class GraphicsImpOGL;

class PostEffectManagerImp : public IPostEffectManager
{
	friend class RenderEngineImp;
	//friend class GraphicsImpOGL;
	//friend class CameraImp;

public:
	PostEffectManagerImp(IGraphics* g);
	virtual ~PostEffectManagerImp() override;

	virtual void SetRenderTargetSize(int width, int height) override;	
	virtual void SetPostEffectOrder(PostEffectType types[], size_t length) override;
	virtual void SetPostEffectTypeEnable(PostEffectType effectType, bool enable) override;
	virtual void AddPostEffect(PostEffectType effectType, BasePostEffect* effect) override;
	virtual BasePostEffect* GetPostEffect(PostEffectType peType) override;
	virtual void SetEnable(bool enable) override { _enable = enable; };
	virtual bool GetEnable() override { return _enable; };

private:
	void _SetupScreenQuad();
	void _SetupRTT(int width, int height);
	void _Render(IRenderTarget* targetRtt);
	void _RenderFinalResult(IRenderTarget* rtt, IRenderTarget* targetRtt);	

private:
	std::map<PostEffectType, BasePostEffect*> _postEffectMap;
	std::vector<PostEffectType> _postEffectOrder;

	IGraphics*		_graphic = nullptr;
	IMaterial*		_material = nullptr;
	IProgram*		_program;
	GLuint			_vertBuffer;
	GLuint			_idxBuffer;
	BaseGeometry*	_screenQuad = nullptr;

	IRenderTarget*	_postEffectRTTs[2];
	int				_rttWidth;
	int				_rttHeight;

	bool			_enable = false;
	bool			_rttReady = false;

	const char* _vsCode =
		"	#version 100\n\
			precision mediump float;\
			\
			attribute vec3 inPos;\
			attribute vec2 inUV;\
			varying vec2 uvCoord;\
			void main()\
			{\
				gl_Position = vec4(inPos.xy, 0.9, 1.0);\
				uvCoord = inUV;\
			}\
		";
	
	const char* _fsCode =
		"	#version 100\n\
			precision mediump float;\
			uniform sampler2D colorMap;\
			varying vec2 uvCoord;\
			\
			void main()\
			{\
				gl_FragData[0] = texture2D(colorMap, uvCoord);\
			}\
		";
};

}
#endif