#pragma once

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cmath>
#include <cstring>

#if _WIN32 || _WIN64 || __linux
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#else
#endif

#include "Definition\EnumDef.h"
#include "Definition\RenderStateDef.h"
#include "Utils\Math3D.h"

#include "Interface\Transform.h"
#include "Interface\IViewCamera.h"
#include "Interface\VertexLayout.h"
#include "Interface\IProgram.h"
#include "Interface\ITexture.h"
#include "Interface\IMaterial.h"
#include "Interface\IRenderTarget.h"
#include "Interface\IGeometry.h"
#include "Interface\IModel.h"
#include "Interface\ISunLight.h"
#include "Interface\ILightManager.h"
#include "Interface\IPostEffectManager.h"
#include "Interface\IGraphics.h"
#include "IRenderEngine.h"

// independent post effects
#include "PostEffect\BasePostEffectTech.h"
#include "PostEffect\Basic\BlurEffect.h"
#include "PostEffect\Basic\BrightnessEffect.h"
#include "PostEffect\Basic\GrayscaleFilter.h"
#include "PostEffect\Basic\BloomEffect.h"

namespace ROOKIES3D
{
	ROOKIES_ENGINE_API IRenderEngine* ROOKIES3D_GetRenderEngineInstance();
	//ROOKIES_ENGINE_API IGraphics* ROOKIES3D_CreateGraphicsHandler();
}