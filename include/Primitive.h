#pragma once

#define MAX_NUM_LIGHT 3
#define MAX_NUM_SPHERE 5
#define MAX_NUM_TRI 300

//------------------------------------------------------------------------
class Light
{
public:

	__host__ __device__ float3	GetColor() const	{ return color;		}
	__host__ __device__ float3	GetPos() const		{ return position;	}
	__host__ __device__ float	GetIa() const		{ return Ia;		}
	__host__ __device__ float	GetId() const		{ return Id;		}
	__host__ __device__ float	GetIs() const		{ return Is;		}

	void				SetColor( float3 rgb )		{ color = rgb;		}
	void				SetPos( float3 pos )		{ position = pos;	}
	void				SetIa( float ia )			{ Ia = ia;			}
	void				SetId( float id )			{ Id = id;			}
	void				SetIs( float is )			{ Is = is;			}

private:

	float3 color;
	float3 position;	
	float Ia,Id,Is;
};


class Sphere;
class Triangle;


//------------------------------------------------------------------------
class Geometry
{
public:

	__host__ __device__ float3	GetColor()				{ return color;			}
	__host__ __device__ float	GetKa()					{ return Ka;			}
	__host__ __device__ float	GetKd()					{ return Kd;			}
	__host__ __device__ float	GetKs()					{ return Ks;			}
	__host__ __device__ float	GetRfrIndex()			{ return rfrIndex;		}
	__host__ __device__ float	GetReflect()			{ return refl_Rate;		}
	__host__ __device__ float	GetRefract()			{ return refr_Rate;		}

	void				SetColor( float3 rgb )		{ color = rgb;			}
	void				SetKa( float ka )			{ Ka = ka;				}
	void				SetKd( float kd )			{ Kd = kd;				}
	void				SetKs( float ks )			{ Ks = ks;				}
	void				SetReflect( float rfl )		{ refl_Rate = rfl;		}
	void				SetRefract( float rfr )		{ refr_Rate = rfr;		}
	void				SetRfrIndex( float n )		{ rfrIndex = n;			}

protected:

	float3 color;
	float Ka,Kd,Ks;
	float refl_Rate;
	float refr_Rate;
	float rfrIndex;
};



//------------------------------------------------------------------------
class Sphere : public Geometry
{
public:

	__host__ __device__ float3	GetCenter()				{ return center;	}
	__host__ __device__ float	GetRadius()				{ return radius;	}

	void				SetCenter( float3 c )		{ center = c;		}
	void				SetRadius( float r )		{ radius = r;		}

private:

	float3 center;
	float radius;
};



//------------------------------------------------------------------------
class Triangle : public Geometry
{
public:

	__host__ __device__ float3	GetVertex1()				{ return v1;					}
	__host__ __device__ float3	GetVertex2()				{ return v2;					}
	__host__ __device__ float3	GetVertex3()				{ return v3;					}
	__host__ __device__ float3	GetNormal()					{ return normal;				}
	
	void				SetVertex1( float3 ver1 )		{ v1 = ver1; ComputeNormal();	}
	void				SetVertex2( float3 ver2 )		{ v2 = ver2; ComputeNormal();	}
	void				SetVertex3( float3 ver3 )		{ v3 = ver3; ComputeNormal();	}


	void ComputeNormal()
	{
		float3 v13 = v3 - v1;
		float3 v12 = v2 - v1;
		normal = cross( v12, v13 );
		normalize( normal );
	}


private:

	float3 v1;
	float3 v2;
	float3 v3;
	float3 normal;
};




//------------------------------------------------------------------------
//__host__ __device__ void Geometry::operator=( Geometry &lhs )
//{
//	color		= lhs.color;
//	Ka			= lhs.Ka;
//	Kd			= lhs.Kd;
//	Ks			= lhs.Ks;
//	refl_Rate	= lhs.refl_Rate;
//	refr_Rate	= lhs.refr_Rate;
//	rfrIndex	= lhs.rfrIndex;
//}
//
//__host__ __device__ void Geometry::operator=( Sphere &lhs )
//{
//	color		= lhs.color;
//	Ka			= lhs.Ka;
//	Kd			= lhs.Kd;
//	Ks			= lhs.Ks;
//	refl_Rate	= lhs.refl_Rate;
//	refr_Rate	= lhs.refr_Rate;
//	rfrIndex	= lhs.rfrIndex;
//}
//
//__host__ __device__ void Geometry::operator=( Triangle &lhs )
//{
//	color		= lhs.color;
//	Ka			= lhs.Ka;
//	Kd			= lhs.Kd;
//	Ks			= lhs.Ks;
//	refl_Rate	= lhs.refl_Rate;
//	refr_Rate	= lhs.refr_Rate;
//	rfrIndex	= lhs.rfrIndex;
//}


struct __align__(32) SceneData
{
	int* num_lights;
	int* num_spheres;
	int* num_triangles;			// 24
	Light* d_lights;
	Sphere* d_spheres;
	Triangle* d_triangles;		//  12

	bool _GPU;

	__host__ void Alloc(bool isOnGPU = true)
	{
		_GPU = isOnGPU;
		if (_GPU)
		{
			cudaMalloc(&d_lights, sizeof(Light)*MAX_NUM_LIGHT);
			cudaMalloc(&d_spheres, sizeof(Sphere)*MAX_NUM_SPHERE);
			cudaMalloc(&d_triangles, sizeof(Triangle)*MAX_NUM_TRI);
			cudaMalloc(&num_lights, sizeof(int));
			cudaMalloc(&num_spheres, sizeof(int));
			cudaMalloc(&num_triangles, sizeof(int));
		}
		else
		{
			d_lights = new Light[MAX_NUM_LIGHT];
			d_spheres = new Sphere[MAX_NUM_SPHERE];
			d_triangles = new Triangle[MAX_NUM_TRI];
			num_lights = new int(0);
			num_spheres = new int(0);
			num_triangles = new int(0);
		}

	}

	__host__ void Release()
	{
		if (_GPU)
		{
			cudaFree(d_lights);
			cudaFree(d_spheres);
			cudaFree(d_triangles);
			cudaFree(num_lights);
			cudaFree(num_spheres);
			cudaFree(num_triangles);
		}
		else
		{
			delete[] d_lights;
			delete[] d_spheres;
			delete[] d_triangles;

			delete num_lights;
			delete num_spheres;
			delete num_triangles;
		}


		d_lights = nullptr;
		d_spheres = nullptr;
		d_triangles = nullptr;
		num_lights = num_spheres = num_triangles = nullptr;
	}

	__host__ void CopyLightsFromCPU(const SceneData& h_scene)
	{
		cudaMemcpy(d_lights, h_scene.d_lights, sizeof(Light)*(*h_scene.num_lights), cudaMemcpyHostToDevice);
		cudaMemcpy(num_lights, h_scene.num_lights, sizeof(int), cudaMemcpyHostToDevice);
	}

	__host__ void CopySpheresFromCPU(const SceneData& h_scene)
	{
		cudaMemcpy(d_spheres, h_scene.d_spheres, sizeof(Sphere)*(*h_scene.num_spheres), cudaMemcpyHostToDevice);
		cudaMemcpy(num_spheres, h_scene.num_spheres, sizeof(int), cudaMemcpyHostToDevice);
	}

	__host__ void CopyTrianglesFromCPU(const SceneData& h_scene)
	{
		cudaMemcpy(d_triangles, h_scene.d_triangles, sizeof(Triangle)*(*h_scene.num_triangles), cudaMemcpyHostToDevice);
		cudaMemcpy(num_triangles, h_scene.num_triangles, sizeof(int), cudaMemcpyHostToDevice);
	}
};