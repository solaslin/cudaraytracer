#pragma once

struct __align__(32) SceneConstant
{
	float3 bgColor;
	float far;			//  16
	float3 eye;
	int screenW;		//  32
	float3 lookAt;	
	int screenH;		//  48
	float3 upVec;
	float near;			//  64
	float3 rightVec;
	float aspect;		//  80	
	float3 origin;		//  96 center of projection plane
	float fov;			// 112
};
