#pragma once

#include <iostream>
#include "cutil_math.h"

typedef enum
{
	AK_EYE,
	AK_LOOK_AT,
	AK_SCREEN_SIZE,
	AK_PERSPECTIVE,
	AK_UP_VEC,
	AK_LIGHT,
	AK_SPHERE,
	AK_TRIANGLE,
	AK_UNKNOWN,
	AK_FINISH
} EAttrKeyword;

class SceneParser
{
public:
	static EAttrKeyword GetNextAttributeType(std::istream& input);
	static bool ParseInt2(std::istream& input, int2& out);
	static bool ParseFloat3(std::istream& input, float3& out);
	static bool ParseFloat4(std::istream& input, float4& out);
};
