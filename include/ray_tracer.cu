﻿#include "cutil_math.h"
#include "SceneConstant.h"
#include "Primitive.h"

#define THREAD_NUM		256
#define BLOCK_NUM		256

#define MAX_RECURSIVE	5
#define AIR_INDICE		1.0
#define PI				3.141592653


__constant__ SceneConstant d_scene;

//------------------------------------------------------------------
struct TraceInfo
{
	// before trace
	int					depth;
	float				accumColorFrac;
	float				curIndex;			// refract index of current medium	
	float3				begin;
	float3				ray;

	// after trace
	float3				normal;
	float3				resultColor;
};


//------------------------------------------------------------------
__device__ float3 CUDAGetRay(float x, float y)
{
	float clipSpaceX = x - (d_scene.screenW / 2);
	float clipSpaceY = (d_scene.screenH / 2) - y;

	float scaleX = 2 * d_scene.near * tan(d_scene.fov * PI / 360) * d_scene.aspect / d_scene.screenW;
	float scaleY = 2 * d_scene.near * tan(d_scene.fov * PI / 360) / d_scene.screenH;

	float3 pointedCoord = d_scene.origin + (clipSpaceX * scaleX * d_scene.rightVec) + (clipSpaceY * scaleY * d_scene.upVec);

	float3 ray = pointedCoord - d_scene.eye;
	ray = normalize(ray);

	return ray;
}

// use barycentric
//------------------------------------------------------------------
__device__ bool CUDACheckIntersect(Triangle &tri, TraceInfo &info, float &dist)
{
	float u, v;

	float3 pt1 = tri.GetVertex1();
	float3 pt2 = tri.GetVertex2();
	float3 pt3 = tri.GetVertex3();

	float3 begin = info.begin;
	float3 ray = info.ray;

	/* find vectors for two edges sharing vert0 */
	float3 edge1 = pt2 - pt1;
	float3 edge2 = pt3 - pt1;

	/* begin calculating determinant - also used to calculate U parameter */
	float3 pvec = cross(ray, edge2);

	/* if determinant is near zero, ray lies in plane of triangle */
	float det = dot(edge1, pvec);

	/* eliminates all the triangles where the value of the determinant is negative */
	if (det < 0.00001 /*&& det > -0.00001*/)		// if frontface: < 0.00001 ;  if backface: > -0.00001
		return false;

	float inv_det = 1.0 / det;

	/* calculate distance from vert0 to ray origin */
	float3 tvec = begin - pt1;


	/* calculate U parameter and test bounds */
	u = dot(tvec, pvec) * inv_det;
	if ((u < 0.0) || (u > 1))
		return false;


	/* prepare to test V parameter */
	float3 qvec = cross(tvec, edge1);

	/* calculate V parameter and test bounds */
	v = dot(ray, qvec) * inv_det;
	if ((v < 0.0) || ((u + v) > 1))
		return false;

	/* calculate t, ray intersects triangle */
	dist = dot(edge2, qvec) * inv_det;

	if (dist < 0.00001)
		return false;

	return true;
}


//------------------------------------------------------------------
__device__ bool CUDATraceTriangle(Triangle &tri, TraceInfo &info, float &dist, Geometry& target)
{
	// trace
	float depth;

	bool result = CUDACheckIntersect(tri, info, depth);

	if (result)
	{
		if (depth > dist)			// farther
		{
			result = false;
		}
		else
		{
			// update min distance;			
			// update intersection --- not needed for now
			//intersection = begin + tuv[ 0 ] * ray;
			dist = depth;
			target = (Geometry)tri;
			info.resultColor = tri.GetColor();
			info.normal = tri.GetNormal();
		}
	}


	return result;
}


// From "Real-Time Rendering" page.741
//------------------------------------------------------------------
__device__ bool CUDATraceSphere(Sphere &sph, TraceInfo &info, float &dist, Geometry& target)
{
	float3 vpc = sph.GetCenter() - info.begin;

	float radiusSquare = sph.GetRadius() * sph.GetRadius();
	float vpcLengthSquare = length(vpc);
	vpcLengthSquare *= vpcLengthSquare;

	float projLength = dot(vpc, info.ray);

	if (projLength < 0)
		return false;

	if (vpcLengthSquare < radiusSquare)
		return false;

	float projDistSquare = vpcLengthSquare - projLength * projLength;
	if (projDistSquare > radiusSquare)
		return false;

	float q = sqrtf(radiusSquare - projDistSquare);

	if (vpcLengthSquare < radiusSquare)
		return false;

	float t = projLength - q;

	if (t > dist)
		return false;

	dist = t;
	target = (Geometry)sph;
	info.resultColor = sph.GetColor();
	info.normal = (info.begin + info.ray * t) - sph.GetCenter();

	return true;
}

//------------------------------------------------------------------
__device__ float3 CUDATraceLight(SceneData& sceneData, const Light& lit, TraceInfo& info, Geometry& obj)
{
	bool hit = false;
	Geometry target;

	float3 point = info.begin;
	float3 L = lit.GetPos() - point;
	float3 N = info.normal;
	float minDist = length(L);				// the max distance of light ray: dist from point to light
	L = normalize(L);						// new direction
	info.ray = L;
	info.begin = point + L * 0.001;

	// check if any intersected sphere
	for (int i = 0; i < *(sceneData.num_spheres); ++i)				
	{
		hit = CUDATraceSphere(sceneData.d_spheres[i], info, minDist, target);
		if (hit)	break;
	}	
	// if no, then check if any intersected triangle
	if (!hit)
	{
		for (int i = 0; i < *(sceneData.num_triangles); ++i)
		{
			hit = CUDATraceTriangle(sceneData.d_triangles[i], info, minDist, target);
			if (hit)	break;
		}
	}

	minDist = length(L);
	float litAtten = fminf(minDist/10.0f, 1.0f);
	litAtten = 1.0f - (litAtten*litAtten);
	float3 outColor = obj.GetColor() * obj.GetKa() * lit.GetIa();

	// if no, then calculate the phong shading
	if (!hit)
	{
		float3 V = d_scene.eye - point;
		V = normalize(V);
		float3 H = L + V;
		H = normalize(H);
		float3 lightColor = (obj.GetColor() * obj.GetKd() * lit.GetId() * fmaxf(dot(L, N), 0.0)) +			// diffuse
							(lit.GetColor() * obj.GetKs() * lit.GetIs() * powf(fmaxf(dot(H, N), 0.0), 100));	// specular

		outColor += lightColor * litAtten;
	}	
	//else
	//{
	//	return obj.GetColor() * obj.GetKa() * lit.GetIa();
	//}
	return outColor;
}


//------------------------------------------------------------------
__device__ void CUDATrace(SceneData& sceneData, TraceInfo* traceInfoArray, int curTrace, int &lastTrace)
{

	TraceInfo *curInfo = &traceInfoArray[curTrace];
	float currentRate = curInfo->accumColorFrac;

	float minDist = d_scene.far;
	bool hit = false;
	Geometry target;

	for (int i = 0; i < *(sceneData.num_triangles); ++i)
		hit = CUDATraceTriangle(sceneData.d_triangles[i], *curInfo, minDist, target) || hit;

	for (int i = 0; i < *(sceneData.num_spheres); ++i)
		hit = CUDATraceSphere(sceneData.d_spheres[i], *curInfo, minDist, target) || hit;
	
	// if no any intersection
	if (!hit)
	{
		curInfo->resultColor = d_scene.bgColor;
		return;
	}
	else
	{
		curInfo->resultColor = make_float3(0.0, 0.0, 0.0);
		curInfo->normal = normalize(curInfo->normal);

		TraceInfo tempInfo;
		tempInfo.normal = curInfo->normal;
		tempInfo.begin = curInfo->begin + (curInfo->ray * minDist);

		for (int i = 0; i < *(sceneData.num_lights); ++i)
		{
			curInfo->resultColor += CUDATraceLight(sceneData, sceneData.d_lights[i], tempInfo, target);
		}

		float rate = fmaxf(1.0 - target.GetReflect() - target.GetRefract(), 0.0);
		curInfo->accumColorFrac = currentRate * rate;
	}

	if (curInfo->depth >= MAX_RECURSIVE)
		return;

	if (target.GetReflect() != 0.0)
	{
		++lastTrace;

		float3 R = reflect(curInfo->ray, curInfo->normal);
		R = normalize(R);

		traceInfoArray[lastTrace].depth = curInfo->depth + 1;
		traceInfoArray[lastTrace].accumColorFrac = currentRate * target.GetReflect();
		traceInfoArray[lastTrace].curIndex = AIR_INDICE;
		traceInfoArray[lastTrace].begin = curInfo->begin + curInfo->ray * minDist + R * 0.001;
		traceInfoArray[lastTrace].ray = R;
	}


	if (target.GetRefract() != 0.0)
	{
		++lastTrace;

		// -------- begin of calculate refraction direction
		float3 norVec = curInfo->normal;

		float n1_n2;// = AIR_INDICE / obj.getRfrIndex();

		// if the current index == the hit-target index, it means we're going out to the air
		if (curInfo->curIndex == target.GetRfrIndex())	n1_n2 = curInfo->curIndex / AIR_INDICE;
		else											n1_n2 = curInfo->curIndex / target.GetRfrIndex();

		// two-sided when refraction
		if (dot(norVec, curInfo->ray) > 0)
			norVec *= -1.0;

		float cos_theta = sqrt(1 - powf(n1_n2, 2) * (1 - powf(dot(norVec, curInfo->ray), 2)));
		float3 T;

		// if theta is near 90 degree --> reflection
		if (cos_theta < 0.1)	T = reflect(curInfo->ray, norVec);
		else					T = curInfo->ray * n1_n2 - norVec * (cos_theta + n1_n2 * dot(norVec, curInfo->ray));

		T = normalize(T);
		// -------- end of calculate refraction direction

		traceInfoArray[lastTrace].depth = curInfo->depth + 1;
		traceInfoArray[lastTrace].accumColorFrac = currentRate * target.GetRefract();
		traceInfoArray[lastTrace].curIndex = target.GetRfrIndex();
		traceInfoArray[lastTrace].begin = curInfo->begin + curInfo->ray * minDist + T * 0.001;
		traceInfoArray[lastTrace].ray = T;
	}
}



//------------------------------------------------------------------
__global__ void CUDAStartTracing(SceneData sceneData, uchar3* color)
{
	const int tid = threadIdx.x;
	const int bid = blockIdx.x;
	int totalPixels = d_scene.screenW * d_scene.screenH;

	int globalThreadID = bid * THREAD_NUM + tid;
	while (globalThreadID < totalPixels)
	{	
		//outColor[globalThreadID] = d_scene.bgColor;
		float3 floatColor = float3{ 0.0f, 0.0f, 0.0f };

		TraceInfo traceInfo[31];
		traceInfo[0].depth = 1;
		traceInfo[0].accumColorFrac = 1.0;
		traceInfo[0].curIndex = AIR_INDICE;
		traceInfo[0].begin = d_scene.eye;
		traceInfo[0].ray = CUDAGetRay(globalThreadID % d_scene.screenW, ceilf(globalThreadID / d_scene.screenW));

		int curTrace = -1;
		int lastTrace = 0;

		while (curTrace != lastTrace)
		{
			++curTrace;
			CUDATrace(sceneData, traceInfo, curTrace, lastTrace);			
			floatColor += traceInfo[curTrace].resultColor * traceInfo[curTrace].accumColorFrac;
		}
		floatColor = fminf(floatColor, float3{ 1.0f, 1.0f , 1.0f });
		color[globalThreadID].x = floatColor.x * 255;
		color[globalThreadID].y = floatColor.y * 255;
		color[globalThreadID].z = floatColor.z * 255;
		
		globalThreadID += THREAD_NUM * BLOCK_NUM;
	}
}

extern "C" void RayTraceCUDA(const SceneData& sceneData, uchar3* color)
{
	CUDAStartTracing <<< BLOCK_NUM, THREAD_NUM >> > (sceneData, color);
}

extern "C" void UpdateSceneConstant(const SceneConstant& h_scene)
{
	cudaMemcpyToSymbol(d_scene, &h_scene, sizeof(SceneConstant));	
	return;
}