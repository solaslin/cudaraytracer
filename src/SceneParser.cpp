#pragma once
#include <string>
#include "SceneParser.h"

EAttrKeyword SceneParser::GetNextAttributeType(std::istream& input)
{
	if (input.eof())					return AK_FINISH;

	std::string token;
	input >> token;
	if		(token == "eye:")			return AK_EYE;
	else if (token == "lookAt:")		return AK_LOOK_AT;
	else if (token == "screen:")		return AK_SCREEN_SIZE;
	else if (token == "perspective:")	return AK_PERSPECTIVE;
	else if (token == "up:")			return AK_UP_VEC;
	else if (token == "light:")			{ std::getline(input, token);	 return AK_LIGHT; }
	else if (token == "sphere:")		{ std::getline(input, token);	 return AK_SPHERE; }
	else if (token == "triangle:")		{ std::getline(input, token);	 return AK_TRIANGLE; }

	return AK_UNKNOWN;
}

bool SceneParser::ParseInt2(std::istream& input, int2& out)
{
	std::string token;

	std::getline(input, token, ',');
	out.x = std::stoi(token);

	std::getline(input, token);
	out.y = std::stoi(token);
	return true;
}

bool SceneParser::ParseFloat3(std::istream& input, float3& out)
{
	std::string token;

	std::getline(input, token, ',');
	out.x = std::stof(token);

	std::getline(input, token, ',');
	out.y = std::stof(token);

	std::getline(input, token);
	out.z = std::stof(token);
	return true;
}

bool SceneParser::ParseFloat4(std::istream& input, float4& out)
{
	std::string token;

	std::getline(input, token, ',');
	out.x = std::stof(token);

	std::getline(input, token, ',');
	out.y = std::stof(token);

	std::getline(input, token, ',');
	out.z = std::stof(token);

	std::getline(input, token);
	out.w = std::stof(token);
	return true;
}
