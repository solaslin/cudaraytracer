#include "RookiesEngine.h"

#include "cutil_math.h"
#include "Primitive.h"
#include "SceneConstant.h"
#include "SceneParser.h"

using namespace ROOKIES3D;
//
// Global variables
//
const int WINDOW_WIDTH = 768;
const int WINDOW_HEIGHT = 768;

GLFWwindow*	_window;

//
using namespace std;

extern "C" void UpdateSceneConstant(const SceneConstant& scene);
extern "C" void RayTraceCUDA(const SceneData& sceneData, uchar3* uColor);

SceneConstant h_SceneConst;
SceneData h_SceneData;
SceneData d_SceneData;

uchar3* h_ucolor;
uchar3* d_ucolor;
bool needUpdateSceneConst = false;
bool needUpdateSceneData = false;

IModel* cameraPivot = nullptr;
IViewCamera* mainCamera = nullptr;
IModel* lightPivot = nullptr;
IModel* lightTransform = nullptr;
IModel* spherePivot = nullptr;
IModel* sphereTransform = nullptr;

//------------------------------------------------------------------
void ReadScene()
{
	h_SceneConst.bgColor = float3{ 0.8f, 0.8f, 0.8f };
	int2 tempInt2;
	float3 tempFloat3;
	float4 tempFloat4;

	cout << "Reading Input...";
	ifstream inFile("../../../resources/data.txt");
	if (!inFile) {
		cerr << "File Could Not Be Opened!!" << endl;
		exit(1);
	}

	int sph_index = 0;
	int tri_index = 0;
	int light_index = 0;

	EAttrKeyword nextAttr = SceneParser::GetNextAttributeType(inFile);
	while (nextAttr != AK_FINISH)
	{
		if (nextAttr == AK_UNKNOWN)
		{
			cerr << "Unknown keyword!!" << endl;
			exit(1);
		}
		else if (nextAttr == AK_EYE)
		{
			SceneParser::ParseFloat3(inFile, tempFloat3);
			h_SceneConst.eye = tempFloat3;
		}
		else if (nextAttr == AK_LOOK_AT)
		{
			SceneParser::ParseFloat3(inFile, tempFloat3);
			h_SceneConst.lookAt = tempFloat3;
		}
		else if (nextAttr == AK_SCREEN_SIZE)
		{
			SceneParser::ParseInt2(inFile, tempInt2);
			h_SceneConst.screenW = tempInt2.x;
			h_SceneConst.screenH = tempInt2.y;
		}
		else if (nextAttr == AK_PERSPECTIVE)
		{
			SceneParser::ParseFloat4(inFile, tempFloat4);
			h_SceneConst.fov = tempFloat4.x;
			h_SceneConst.aspect = tempFloat4.y;
			h_SceneConst.near = tempFloat4.z;
			h_SceneConst.far = tempFloat4.w;
		}
		else if (nextAttr == AK_UP_VEC)
		{
			SceneParser::ParseFloat3(inFile, tempFloat3);
			h_SceneConst.upVec = tempFloat3;
		}
		else if (nextAttr == AK_LIGHT)
		{
			Light& light = h_SceneData.d_lights[light_index++];
			SceneParser::ParseFloat3(inFile, tempFloat3);	light.SetPos(tempFloat3);
			SceneParser::ParseFloat3(inFile, tempFloat3);	light.SetColor(tempFloat3);
			SceneParser::ParseFloat3(inFile, tempFloat3);	light.SetIa(tempFloat3.x);	light.SetId(tempFloat3.y);	light.SetIs(tempFloat3.z);
		}
		else if (nextAttr == AK_SPHERE)
		{
			Sphere& sphere = h_SceneData.d_spheres[sph_index++];
			SceneParser::ParseFloat4(inFile, tempFloat4);	sphere.SetCenter(make_float3(tempFloat4));	sphere.SetRadius(tempFloat4.w);
			SceneParser::ParseFloat3(inFile, tempFloat3);	sphere.SetColor(tempFloat3);
			SceneParser::ParseFloat3(inFile, tempFloat3);	sphere.SetKa(tempFloat3.x);	sphere.SetKd(tempFloat3.y);	sphere.SetKs(tempFloat3.z);
			SceneParser::ParseFloat3(inFile, tempFloat3);	sphere.SetReflect(tempFloat3.x);	sphere.SetRefract(tempFloat3.y);	sphere.SetRfrIndex(tempFloat3.z);
		}
		else if (nextAttr == AK_TRIANGLE)
		{
			Triangle& triangle = h_SceneData.d_triangles[tri_index++];
			SceneParser::ParseFloat3(inFile, tempFloat3);	triangle.SetVertex1(tempFloat3);
			SceneParser::ParseFloat3(inFile, tempFloat3);	triangle.SetVertex2(tempFloat3);
			SceneParser::ParseFloat3(inFile, tempFloat3);	triangle.SetVertex3(tempFloat3);
			SceneParser::ParseFloat3(inFile, tempFloat3);	triangle.SetColor(tempFloat3);
			SceneParser::ParseFloat3(inFile, tempFloat3);	triangle.SetKa(tempFloat3.x);	triangle.SetKd(tempFloat3.y);	triangle.SetKs(tempFloat3.z);
			SceneParser::ParseFloat3(inFile, tempFloat3);	triangle.SetReflect(tempFloat3.x);	triangle.SetRefract(tempFloat3.y);	triangle.SetRfrIndex(tempFloat3.z);
		}
		nextAttr = SceneParser::GetNextAttributeType(inFile);
	}

	*h_SceneData.num_lights = light_index;
	*h_SceneData.num_spheres = sph_index;
	*h_SceneData.num_triangles = tri_index;

	inFile.close();
	std::cout << "Done" << endl;
}

//------------------------------------------------------------------
void InitializeRes()
{
	h_SceneData.Alloc(false);
	d_SceneData.Alloc(true);
	// read geometries and scene
	ReadScene();	
	h_ucolor = (uchar3*)malloc(sizeof(uchar3) * h_SceneConst.screenW * h_SceneConst.screenH);
	cudaMalloc((void**)&d_ucolor, sizeof(uchar3) * h_SceneConst.screenW * h_SceneConst.screenH);
}

void ReleaseRes()
{
	h_SceneData.Release();
	d_SceneData.Release();
	free(h_ucolor);
	cudaFree(d_ucolor);	
	h_ucolor = nullptr;
	d_ucolor = nullptr;
}

void UpdateSceneData(const SceneData& sceneData_cpu)
{
	d_SceneData.CopyLightsFromCPU(sceneData_cpu);
	d_SceneData.CopySpheresFromCPU(sceneData_cpu);
	d_SceneData.CopyTrianglesFromCPU(sceneData_cpu);
}


void ComputePlane()
{
	float3 normal = h_SceneConst.eye - h_SceneConst.lookAt;
	normal = normalize(normal);

	h_SceneConst.origin = h_SceneConst.eye - normal * h_SceneConst.near;

	h_SceneConst.rightVec = cross(h_SceneConst.upVec, normal);
	h_SceneConst.rightVec = normalize(h_SceneConst.rightVec);

	h_SceneConst.upVec = cross(normal, h_SceneConst.rightVec);
	h_SceneConst.upVec = normalize(h_SceneConst.upVec);
}

void RayTrace()
{
	// run kernel	
	RayTraceCUDA(d_SceneData, d_ucolor);
	cudaMemcpy(h_ucolor, d_ucolor, sizeof(uchar3) * h_SceneConst.screenW * h_SceneConst.screenH, cudaMemcpyDeviceToHost);
}

//------------------------------------------------------------------
bool _ClosingWindow()
{
	bool closing = glfwWindowShouldClose(_window) != 0;
	return closing;
}

void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_F1 && action == GLFW_REPEAT)
	{
		Vector3 vec3;
		cameraPivot->GetTransform()->WorldRotateDeltaAroundY(1);
		mainCamera->GetTransform()->GetWorldPosition(vec3);
		h_SceneConst.eye = float3{ vec3[0], vec3[1], vec3[2] };
		needUpdateSceneConst = true;
	}
	else if (key == GLFW_KEY_F2 && action == GLFW_REPEAT)
	{
		Vector3 vec3;
		cameraPivot->GetTransform()->WorldRotateDeltaAroundY(-1);
		mainCamera->GetTransform()->GetWorldPosition(vec3);
		h_SceneConst.eye = float3{ vec3[0], vec3[1], vec3[2] };
		needUpdateSceneConst = true;
	}
	else if (key == GLFW_KEY_F3 && action == GLFW_REPEAT)
	{
		Vector3 vec3;
		lightPivot->GetTransform()->WorldRotateDeltaXYZ(0, 2, 2);
		lightTransform->GetTransform()->GetWorldPosition(vec3);
		h_SceneData.d_lights[0].SetPos(float3{ vec3[0], vec3[1], vec3[2] });
		needUpdateSceneData = true;
	}
	else if (key == GLFW_KEY_F4 && action == GLFW_REPEAT)
	{
		Vector3 vec3;
		lightPivot->GetTransform()->WorldRotateDeltaXYZ(0, -2, -2);
		lightTransform->GetTransform()->GetWorldPosition(vec3);
		h_SceneData.d_lights[0].SetPos(float3{ vec3[0], vec3[1], vec3[2] });
		needUpdateSceneData = true;
	}
	else if (key == GLFW_KEY_F5 && action == GLFW_REPEAT)
	{
		Vector3 vec3;
		spherePivot->GetTransform()->WorldRotateDeltaAroundY(2);
		sphereTransform->GetTransform()->GetWorldPosition(vec3);
		h_SceneData.d_spheres[0].SetCenter(float3{ vec3[0], vec3[1], vec3[2] });
		needUpdateSceneData = true;
	}
	else if (key == GLFW_KEY_F6 && action == GLFW_REPEAT)
	{
		Vector3 vec3;
		spherePivot->GetTransform()->WorldRotateDeltaAroundY(-2);
		sphereTransform->GetTransform()->GetWorldPosition(vec3);
		h_SceneData.d_spheres[0].SetCenter(float3{ vec3[0], vec3[1], vec3[2] });
		needUpdateSceneData = true;
	}
}

//------------------------------------------------------------------
int main()
{
	InitializeRes();

	// first launch
	ComputePlane();
	UpdateSceneConstant(h_SceneConst);
	UpdateSceneData(h_SceneData);		
	RayTrace();

	/////////////////////////////////////////////////////
	// Render Engine
	/////////////////////////////////////////////////////
	IRenderEngine* renderEngine = ROOKIES3D_GetRenderEngineInstance();
	_window = renderEngine->CreateContext(WINDOW_WIDTH, WINDOW_HEIGHT);

	IGraphics* graphics = renderEngine->GetGraphicsHandler();
	glfwSetKeyCallback(_window, KeyCallback);

	// main camera
	cameraPivot = renderEngine->CreateEmptyModel();
	mainCamera = renderEngine->CreateCamera();
	mainCamera->SetProjType(EProjType::PT_ORTHO);
	mainCamera->SetFar(500.0f);
	mainCamera->SetNear(-500.0f);
	mainCamera->SetViewPort((float)WINDOW_WIDTH, (float)WINDOW_HEIGHT);
	mainCamera->GetTransform()->SetWorldPosition(Vector3(h_SceneConst.eye.x, h_SceneConst.eye.y, h_SceneConst.eye.z));
	mainCamera->GetTransform()->SetParent(cameraPivot->GetTransform(), true);
	renderEngine->SetMainCamera(mainCamera);

	// light 1
	lightPivot = renderEngine->CreateEmptyModel();
	lightTransform = renderEngine->CreateEmptyModel();
	float3 lightPos = h_SceneData.d_lights[0].GetPos();
	lightTransform->GetTransform()->SetWorldPosition(Vector3(lightPos.x, lightPos.y, lightPos.z));
	lightTransform->GetTransform()->SetParent(lightPivot->GetTransform(), true);

	// sphere 1
	spherePivot = renderEngine->CreateEmptyModel();
	sphereTransform = renderEngine->CreateEmptyModel();
	float3 spherePos = h_SceneData.d_spheres[0].GetCenter();
	sphereTransform->GetTransform()->SetWorldPosition(Vector3(spherePos.x, spherePos.y, spherePos.z));
	sphereTransform->GetTransform()->SetParent(spherePivot->GetTransform(), true);


	//////////////////////////////////////////////////////////////////////////
	// create fullscreen mesh and texture and shader
	//////////////////////////////////////////////////////////////////////////
	IProgram* shader = graphics->CreateShaderProgramFromFile("../../../resources/pasteScreen.vs", "../../../resources/default_tex.fs");
	ITexture* tex = graphics->CreateRenderTexture(h_SceneConst.screenW, h_SceneConst.screenH)->GetColorTexture();
	tex->UploadTexture(h_SceneConst.screenW, h_SceneConst.screenH, 3, (GLubyte*)(h_ucolor));

	IMaterial* mat = renderEngine->CreateMaterial();
	IModel* fullScreen = renderEngine->CreateEmptyModel();
	mat->BindTexture("colorMap", tex);
	mat->BindProgram(shader);
	fullScreen->SetGeometry(renderEngine->GetScreenQuad(true));
	fullScreen->SetMaterial(mat);

	// ----------------------
	// Rendering frame
	// ----------------------
	while (!_ClosingWindow())
	{
		if (needUpdateSceneData || needUpdateSceneConst)
		{
			if (needUpdateSceneConst)
			{
				ComputePlane();
				UpdateSceneConstant(h_SceneConst);
				needUpdateSceneConst = false;
			}
			if (needUpdateSceneData)
			{
				d_SceneData.CopyLightsFromCPU(h_SceneData);
				d_SceneData.CopySpheresFromCPU(h_SceneData);
				needUpdateSceneData = false;
			}

			RayTrace();
			tex->UploadTexture(h_SceneConst.screenW, h_SceneConst.screenH, 3, (GLubyte*)(h_ucolor));
		}

		// must!
		renderEngine->BeginRender();

		fullScreen->Draw();

		// must!
		renderEngine->EndRender();
	}

	ReleaseRes();
	graphics = nullptr;
	delete renderEngine;
	return 0;
}